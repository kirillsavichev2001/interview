const findBigAndSmallString = (firstString, secondString) => {
  const stringArr = []

  if (firstString.length > secondString.length) {
    stringArr.push(firstString)
    stringArr.push(secondString)
  } else {
    stringArr.push(secondString)
    stringArr.push(firstString)
  }

  return stringArr
};

const findSequence = (firstString, secondString) => {
  const [bigString, smallString] = findBigAndSmallString(firstString, secondString)

  let iterationCount = 1
  let step = smallString.length - iterationCount

  while (iterationCount <= smallString.length) {
    for (let index = 0; index < iterationCount; index++) {
      let nextIndex = index + step;
      const string = smallString.slice(index, nextIndex + 1)
      if (bigString.indexOf(string) !== -1) {
        return string
      }
    }
    iterationCount += 1
    step = smallString.length - iterationCount
  }
};

const firstString = 'aababba'
const secondString = 'abbaabcd'

const answer = findSequence(firstString, secondString)

console.log("answer", {
  firstString,
  secondString,
  answer,
})
