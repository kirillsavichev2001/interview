import React, {useEffect} from "react";
import classes from './App.module.css'

const Field = (props) => {
  const {title, value, onChange} = props
  return (
    <div className={classes.checkBoxContainer}>
      <input checked={value} onChange={onChange} type={'checkbox'}/>
      {value && <p className={classes.checkBoxTitle}>{title}</p>}
    </div>
  );
};

const AppContainer = () => {
  const [globalValue, setGlobalValue] = React.useState(false);
  const [checkboxState, setCheckboxState] = React.useState([
    {id: 0, value: false, title: 'CheckBox - 0'},
    {id: 1, value: false, title: 'CheckBox - 1'},
    {id: 2, value: false, title: 'CheckBox - 2'},
    {id: 3, value: false, title: 'CheckBox - 3'},
  ]);

  useEffect(() => {
    const allIsTrue = checkboxState.every(checkBoxItem => checkBoxItem.value)
    setGlobalValue(allIsTrue)
  }, [checkboxState])

  const onAllControlCheckboxChange = React.useCallback(() => {
    const newGlobalValue = !globalValue;

    setGlobalValue(newGlobalValue)

    setCheckboxState(state => state.map(checkBoxItem => ({
      ...checkBoxItem, value: newGlobalValue
    })))
  }, [globalValue]);

  const onChangeCheckBox = React.useCallback((id) => {
    setCheckboxState(state => state.map(checkBoxItem => {
      const newValue =
        checkBoxItem.id === id
          ? !checkBoxItem.value
          : checkBoxItem.value;

      return ({...checkBoxItem, value: newValue});
    }))
  }, []);

  return (
    <AppContent
      {...{
        onChangeCheckBox,
        checkboxState,
        onAllControlCheckboxChange,
        globalValue,
      }}
    />
  )
}

const AppContent = (props) => {
  const {
    onChangeCheckBox,
    checkboxState,
    onAllControlCheckboxChange,
    globalValue,
  } = props

  const renderItem = React.useCallback(({id, title, value}) => {
    const onChange = () => onChangeCheckBox(id);
    return <Field key={id} onChange={onChange} value={value} title={title}/>
  }, [checkboxState]);


  return (
    <div className="App">
      {checkboxState.map(renderItem)}
      <div className={classes.checkBoxContainer}>
        <input
          onChange={onAllControlCheckboxChange}
          checked={globalValue}
          type={'checkbox'}
        />
        <p className={classes.checkBoxTitle}>All Control</p>
      </div>

    </div>
  );
}

export default AppContainer;
