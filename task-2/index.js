const shiftArrayElement = (arr, shift) => {
  const len = arr.length

  return arr.map((_, i, array) => {
    const index = (i + len - shift % len) % len
    return array[index]
  })
};

const array = [100, 200, 300, 400, 500];
const shift = 3;

const answer = shiftArrayElement(array, shift);

console.log("answer", answer)
