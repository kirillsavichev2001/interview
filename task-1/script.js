const button = document.getElementById('open-menu-button')
const menu = document.getElementById('menu')

let menuIsOpen = window.innerWidth >= 1200

menu.classList.add(menuIsOpen ? 'menu-open' : 'menu-close')

button.addEventListener('click', () => {
  menuIsOpen = !menuIsOpen
  if (menuIsOpen) {
    menu.classList.add('menu-open')
    menu.classList.remove('menu-close')
  } else {
    menu.classList.add('menu-close')
    menu.classList.remove('menu-open')
  }
})
